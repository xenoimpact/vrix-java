package vrix.concurrent;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * AutoCloseable implementation for Lock object in try..catch statement.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public class AcLock implements Lock, AutoCloseable {
    /**
     * Returns Auto-closeable lock object.
     *
     * @param lock Original lock object.
     * @return Auto-closeable lock object.
     */
    public static AcLock of(final Lock lock) {
        return new AcLock(lock);
    }

    /**
     * Constructs Auto-closeable lock object.
     *
     * @param lock Original lock object.
     */
    private AcLock(final Lock lock) {
        if (lock == null) throw new NullPointerException("`lock` must not be null.");
        this.lock = lock;
    }

    private final Lock lock;

    @Override
    public void close() throws Exception {
        unlock();
    }

    @Override
    public void lock() {
        lock.lock();
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        lock.lockInterruptibly();
    }

    @Override
    public boolean tryLock() {
        return lock.tryLock();
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return lock.tryLock(time, unit);
    }

    @Override
    public void unlock() {
        lock.unlock();
    }

    @Override
    public Condition newCondition() {
        return lock.newCondition();
    }
}
