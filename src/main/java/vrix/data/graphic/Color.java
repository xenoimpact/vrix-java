package vrix.data.graphic;

import vrix.util.O;
import vrix.util.StringU;
import vrix.util.V;

/**
 * 색상.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public class Color {
    public Color(String rgb) {
        this(rgb, 255);
    }

    public Color(String rgb, int alpha) {
        V.noEmpty(rgb, "rgb");
        V.noLess(rgb.length(), 6, "rgb.length()");
        rgb = rgb.substring(rgb.length() - 6);

        init(
                Integer.valueOf(rgb.substring(0, 2), 16),
                Integer.valueOf(rgb.substring(2, 4), 16),
                Integer.valueOf(rgb.substring(4, 6), 16),
                alpha
        );
    }

    public Color(int red, int green, int blue) {
        init(red, green, blue, 255);
    }

    public Color(int red, int green, int blue, int alpha) {
        init(red, green, blue, alpha);
    }

    private int rgb;
    private int rgba;
    private int argb;
    private String hex;
    private int red;
    private int green;
    private int blue;
    private int alpha;

    private void init(final int red, final int green, final int blue, final int alpha) {
        this.red = clamp(red);
        this.green = clamp(green);
        this.blue = clamp(blue);
        this.alpha = clamp(alpha);
        this.rgb = (this.red << 16) | (this.green << 8) | this.blue;
        this.rgba = (this.red << 24) | (this.green << 16) | (this.blue << 8) | this.alpha;
        this.argb = (this.alpha << 24) | (this.red << 16) | (this.green << 8) | this.blue;
        this.hex = new StringBuilder(StringU.lpad(Integer.toHexString(this.red), 2, '0'))
                .append(StringU.lpad(Integer.toHexString(this.green), 2, '0'))
                .append(StringU.lpad(Integer.toHexString(this.blue), 2, '0'))
                .toString();
    }

    private int clamp(final int v) {
        if (v < 0) return 0;
        if (v > 255) return 255;
        return v;
    }

    public int rgb() {
        return rgb;
    }

    public int rgba() {
        return rgba;
    }

    public int argb() {
        return argb;
    }

    public String hex() {
        return hex;
    }

    public String hex(final String prefix) {
        if (O.empty(prefix)) return hex;
        return prefix + hex;
    }

    public int red() {
        return red;
    }

    public int green() {
        return green;
    }

    public int blue() {
        return blue;
    }

    public int alpha() {
        return alpha;
    }
}
