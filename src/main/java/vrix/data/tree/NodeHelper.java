package vrix.data.tree;

import java.util.List;

/**
 * 트리 구성을 위한 요소 정보 지원자.
 *
 * @param <NODE> 요소 클래스.
 * @param <ID> 요소 ID 클래스.
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public interface NodeHelper<NODE, ID> {
    /**
     * 특정 요소의 ID를 반환한다.
     *
     * @param node 대상 요소.
     * @return ID.
     */
    ID id(NODE node);

    /**
     * 특정 요소의 부모 요소 ID를 반환한다.
     *
     * @param node 대상 요소.
     * @return 부모 요소 ID.
     */
    ID parentId(NODE node);

    /**
     * 특정 요소가 최상위 요소인지 판정한다.
     *
     * @param node 대상 요소.
     * @return 최상위 요소 여부.
     */
    boolean isRoot(NODE node);

    /**
     * 특정 요소의 자식 요소 목록을 반환한다.
     *
     * @param node 대상 요소.
     * @return 자식 요소 목록.
     */
    List<NODE> children(NODE node);

    /**
     * 특정 요소의 자식 요소 목록을 지정한다.
     *
     * @param node 대상 요소.
     * @param children 자식 요소 목록,
     */
    void children(NODE node, List<NODE> children);
}
