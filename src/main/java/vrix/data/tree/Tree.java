package vrix.data.tree;

import org.slf4j.Logger;
import vrix.util.LogHelper;
import vrix.util.O;
import vrix.util.StringU;
import vrix.util.V;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static vrix.data.tree.Tree.OrphanPolicy.DROP;
import static vrix.data.tree.Tree.OrphanPolicy.ROOT;

/**
 * 트리 객체.
 *
 * @param <NODE> 요소 클래스.
 * @param <ID>   요소 ID 클래스.
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public class Tree<NODE, ID> implements NodeHelper<NODE, ID> {
    /**
     * 부모가 없는 요소 배치 정책.
     */
    public enum OrphanPolicy {
        /**
         * 최상위 요소로 추가.
         */
        ROOT,
        /**
         * 최상위 요소로 추가하되, 가장 앞 순서로 배치.
         */
        UNSHIFT_ROOT,
        /**
         * 배치하지 않음.
         */
        DROP
    }

    private static final Logger L = LogHelper.get();

    /**
     * 기본으로 사용되는 부모가 없는 요소 배치 정책.
     */
    public static OrphanPolicy DEFAULT_ORPHAN_POLICY = ROOT;

    /**
     * 요소 목록으로부터 트리를 구성하되,
     * 부모가 없는 요소 배치 정책은 기본 정책을 사용한다.
     *
     * @param nodes      요소 목록.
     * @param nodeHelper 요소 정보 지원자.
     * @param <NODE>     요소 클래스.
     * @param <ID>       요소 ID 클래스.
     * @return 트리.
     * @throws IllegalArgumentException
     */
    public static <NODE, ID> Tree<NODE, ID> from(final Collection<NODE> nodes,
                                                 final NodeHelper<NODE, ID> nodeHelper)
            throws IllegalArgumentException {
        return from(nodes, nodeHelper, DEFAULT_ORPHAN_POLICY);
    }

    /**
     * 요소 목록으로부터 트리를 구성한다.
     *
     * @param nodes        요소 목록.
     * @param nodeHelper   요소 정보 지원자.
     * @param orphanPolicy 부모가 없는 요소 배치 정책.
     * @param <NODE>       요소 클래스.
     * @param <ID>         요소 ID 클래스.
     * @return 트리.
     * @throws IllegalArgumentException
     */
    public static <NODE, ID> Tree<NODE, ID> from(final Collection<NODE> nodes,
                                                 final NodeHelper<NODE, ID> nodeHelper,
                                                 final OrphanPolicy orphanPolicy)
            throws IllegalArgumentException {
        V.noNull(nodeHelper, "nodeHelper");

        final Tree<NODE, ID> tree = new Tree(nodeHelper);

        L.debug("`Tree` building...");

        if (O.empty(nodes)) {
            L.debug("Returns empty `Tree`.");
            return tree;
        }

        final Map<ID, NODE> idToNode = tree.idToNode;
        final List<NODE> root = tree.root;

        //TODO solve concurrent issue in future
        final Map<ID, List<NODE>> idToChildren = new HashMap<ID, List<NODE>>();
        final List<NODE> orphans = new LinkedList<NODE>();

        for (NODE node : nodes) {
            if (node == null) {
                L.debug("  Null node was ignored.");
                continue;
            }

            final ID id = nodeHelper.id(node);

            if (id == null) {
                L.warn("  Null ID was ignored");
                continue;
            }

            // ID 중복 방지.
            if (idToNode.containsKey(id)) throw new IllegalArgumentException(
                    new StringBuilder("ID(").append(id).append(") is duplicated.")
                            .append(StringU.lineSeparator())
                            .append("  0: ").append(String.valueOf(idToNode.get(id)))
                            .append(StringU.lineSeparator())
                            .append("  1: ").append(String.valueOf(node))
                            .toString()
            );

            // 등록.
            idToNode.put(id, node);

            // 미리 저장되던 자식 요소 목록 전달.
            if (idToChildren.containsKey(id)) {
                nodeHelper.children(node, idToChildren.get(id));

                // 미리 저장되던 자식 요소들에게 부모가 지정되었으므로,
                // 부모가 없는 요소 목록에서 제거.
                for (NODE notOrphan : idToChildren.get(id)) {
                    orphans.remove(notOrphan);
                }
            }

            if (nodeHelper.isRoot(node)) {
                root.add(node); // 최상위 요소.
            } else {
                final ID parentId = nodeHelper.parentId(node);

                if (parentId == null) {
                    // 부모가 없는 요소 저장.
                    orphans.add(node);
                } else {
                    // 부모가 있어야 하는 요소.
                    final List<NODE> bros;

                    if (idToChildren.containsKey(parentId)) bros = idToChildren.get(parentId);
                    else {
                        bros = new CopyOnWriteArrayList<NODE>();
                        idToChildren.put(parentId, bros);
                    }

                    bros.add(node);

                    if (idToNode.containsKey(parentId)) {
                        // 부모가 이미 식별되어 있음.
                        final NODE parent = idToNode.get(parentId);
                        if (nodeHelper.children(parent) != bros) nodeHelper.children(parent, bros);
                    } else {
                        // 부모가 아직 식별되지 않음.
                        orphans.add(node);
                    }
                }
            }
        }

        if (!orphans.isEmpty() && (orphanPolicy != DROP)) {
            // 부모가 없는 요소 배치.
            switch (orphanPolicy) {
                case ROOT:
                    root.addAll(orphans);
                    break;
                case UNSHIFT_ROOT:
                    root.addAll(0, orphans);
                    break;
            }
        }

        L.debug("`Tree` has been built successfully.");

        return tree;
    }

    /**
     * 생성자.
     *
     * @param nodeHelper 부모가 없는 요소 배치 정책.
     */
    Tree(final NodeHelper<NODE, ID> nodeHelper) {
        super();
        V.noNull(nodeHelper, "nodeHelper");
        this.nodeHelper = nodeHelper;
    }

    private final Map<ID, NODE> idToNode = new ConcurrentHashMap<ID, NODE>();
    private final List<NODE> root = new CopyOnWriteArrayList<NODE>();
    private final Map<NODE, List<NODE>> nodeToLeaf = new ConcurrentHashMap<NODE, List<NODE>>();

    private final NodeHelper<NODE, ID> nodeHelper;

    /**
     * 최상위 요소 목록을 반환한다.
     *
     * @return 최상위 요소 목록.
     */
    public List<NODE> root() {
        return root;
    }

    /**
     * 특정 요소를 포함하고 있는지 판정한다.
     *
     * @param node 탐색할 요소.
     * @return 포함 여부.
     */
    public boolean contains(final NODE node) {
        if (node == null) return false;
        return idToNode.containsValue(node);
    }

    /**
     * 기준 요소로부터 하위 트리에 특정 요소를 포함하고 있는지 판정한다.
     *
     * @param ancestor 기준 요소.
     * @param node     탐색할 요소.
     * @return 포함 여부.
     */
    public boolean contains(final NODE ancestor, final NODE node) {
        if (!contains(ancestor) || !contains(node)) return false;

        final int ancestorHashCode = ancestor.hashCode();
        NODE parent = parent(node);
        while (parent != null) {
            if (ancestorHashCode == parent.hashCode() && ancestor.equals(parent)) return true;
            parent = parent(parent);
        }

        return false;
    }

    /**
     * ID에 대응하는 요소를 반환한다.
     *
     * @param id ID.
     * @return 요소.
     */
    public NODE get(final ID id) {
        if (id == null) return null;
        return idToNode.get(id);
    }

    /**
     * 특정 요소의 부모 요소를 반환한다.
     *
     * @param node 대상 요소.
     * @return 부모 요소.
     */
    public NODE parent(final NODE node) {
        if (node == null) return null;
        return get(parentId(node));
    }

    /**
     * 전체 최하위 요소 목록을 반환한다.
     *
     * @return 전체 최하위 요소 목록.
     */
    public List<NODE> leaf() {
        final List<NODE> leaf = new ArrayList<NODE>();

        for (NODE rootNode : root) {
            leaf.addAll(leaf(rootNode));
        }

        return leaf;
    }

    /**
     * 기준 요소의 최하위 요소 목록을 반환한다.
     *
     * @param node 기준 요소.
     * @return 최하위 요소 목록.
     */
    public List<NODE> leaf(final NODE node) {
        if (nodeToLeaf.containsKey(node)) return nodeToLeaf.get(node);

        final List<NODE> result = new ArrayList<NODE>();
        final List<NODE> children = nodeHelper.children(node);
        if (!O.empty(children)) {
            for (NODE childNode : children) {
                result.addAll(leaf(childNode));
            }
        } else result.add(node);

        nodeToLeaf.put(node, result);

        return result;
    }

    /**
     * 최상위 요소 목록을 기준으로 트리 구성을 다시 읽어들인다.
     */
    public void reload() {
        nodeToLeaf.clear();
        idToNode.clear();

        registerNodes(root);
    }

    private void registerNodes(final List<NODE> nodes) {
        for (NODE node : nodes) {
            final ID id = nodeHelper.id(node);

            // ID 중복 방지.
            if (idToNode.containsKey(id)) throw new IllegalArgumentException(
                    new StringBuilder("ID(").append(id).append(") is duplicated.")
                            .append(StringU.lineSeparator())
                            .append("  0: ").append(String.valueOf(idToNode.get(id)))
                            .append(StringU.lineSeparator())
                            .append("  1: ").append(String.valueOf(node))
                            .toString()
            );

            idToNode.put(id, node);

            final List<NODE> children = nodeHelper.children(node);
            if (!O.empty(children)) registerNodes(children);
        }
    }

    @Override
    public ID id(NODE node) {
        return nodeHelper.id(node);
    }

    @Override
    public ID parentId(NODE node) {
        return nodeHelper.parentId(node);
    }

    @Override
    public boolean isRoot(NODE node) {
        return root().contains(node);
    }

    @Override
    public List<NODE> children(final NODE node) {
        return nodeHelper.children(node);
    }

    @Deprecated
    @Override
    public void children(NODE node, List<NODE> children) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("This method is not supported.");
    }
}
