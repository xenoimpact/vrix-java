package vrix.support.mybatis.plugin;

import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.plugin.*;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * MyBatis 컬럼 정보 플러그인.
 *
 * ```
 * configuration.addInterceptor(new MyBatisColumnNames());
 *
 * ...
 *
 * MyBatisColumnNames.columnNames()
 * ```
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
@Intercepts({
        @Signature(type = ResultSetHandler.class, method = "handleResultSets", args = {Statement.class})
})
public class MyBatisColumnNames implements Interceptor {
    private static final ThreadLocal<List<String>> columnNames = new ThreadLocal<List<String>>();

    /**
     * @return 현재 Thread 의 마지막 컬럼명 목록.
     */
    public static final List<String> columnNames() {
        return columnNames.get();
    }

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        final Object[] args = invocation.getArgs();
        if ((args == null) || (args.length == 0) ||
                (args[0] == null) || !(args[0] instanceof Statement)) return invocation.proceed();

        final Statement statement = (Statement) args[0];

        ResultSet rs = statement.getResultSet();

        while ((rs == null) && statement.getMoreResults()) {
            rs = statement.getResultSet();
        }

        if (rs != null) {
            final ResultSetMetaData rsmd = rs.getMetaData();

            final int columnCount = rsmd.getColumnCount();
            final List<String> columnNames = new ArrayList<String>();

            for (int i = 1; i <= columnCount; ++i) {
                columnNames.add(rsmd.getColumnName(i));
            }

            MyBatisColumnNames.columnNames.set(columnNames);
        } else {
            MyBatisColumnNames.columnNames.set(new ArrayList<String>());
        }

        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
