package vrix.support.mybatis.type;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.*;
import java.util.*;

/**
 * MyBatis type handler for `java.util.Calendar`.
 *
 * ```
 * configuration.getTypeHandlerRegistry().register(CalendarTypeHandler.class);
 * ```
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public class CalendarTypeHandler extends BaseTypeHandler<Calendar> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Calendar calendar, JdbcType jdbcType) throws SQLException {
        ps.setTimestamp(i, new Timestamp(calendar.getTimeInMillis()));
    }

    @Override
    public Calendar getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return sqlTimestampToCalendar(rs.getTimestamp(columnName));
    }

    @Override
    public Calendar getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return sqlTimestampToCalendar(rs.getTimestamp(columnIndex));
    }

    @Override
    public Calendar getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return sqlTimestampToCalendar(cs.getTimestamp(columnIndex));
    }

    private Calendar sqlTimestampToCalendar(final Timestamp sqlTimestamp) {
        if (sqlTimestamp == null) return null;
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(sqlTimestamp.getTime());

        return calendar;
    }
}
