package vrix.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 로그 관련 지원 기능 모음.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public final class LogHelper {
    /**
     * 현재 클래스에 해당하는 로거 객체를 반환한다.
     *
     * @return 로거.
     * @throws IllegalStateException
     */
    public static final Logger get() throws IllegalStateException {
        final StackTraceElement[] stackTraces = Thread.currentThread().getStackTrace();
        if (stackTraces.length > 2) return LoggerFactory.getLogger(stackTraces[2].getClassName());

        final StringBuilder message = new StringBuilder("Could not found Logger.");

        for (StackTraceElement stackTrace : stackTraces) {
            message.append(StringU.lineSeparator())
                    .append("  -> ")
                    .append(stackTrace.getClassName()).append(".")
                    .append(stackTrace.getMethodName()).append("(")
                    .append(stackTrace.getFileName()).append(":")
                    .append(stackTrace.getLineNumber()).append(")");
        }

        throw new IllegalStateException(message.toString());
    }

    private LogHelper() {}
}
