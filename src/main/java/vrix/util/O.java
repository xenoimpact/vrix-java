package vrix.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

/**
 * 객체 관련 기능 모음.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public final class O {
    /**
     * 배열 생성.
     *
     * @param type 배열 요소 유형.
     * @param elements 요소들.
     * @param <T> 배열 요소 유형.
     * @return 배열.
     */
    public static final <T> T[] array(final Class<T> type, final T... elements) {
        final int n = elements.length;
        final T[] array = (T[]) Array.newInstance(type, n);
        System.arraycopy(elements, 0, array, 0, n);
        return array;
    }

    /**
     * 빈 객체인지 판별한다.
     * null인 경우, 빈 객체로 판단.
     *
     * @param target 판별 대상.
     * @return 빈 객체인지 여부.
     */
    public static final boolean empty(final Object target) {
        if (target == null) return true;

        if (CharSequence.class.isInstance(target)) return (((CharSequence) target).length() == 0);
        if (Collection.class.isInstance(target)) return ((Collection) target).isEmpty();
        if (Map.class.isInstance(target)) return ((Map) target).isEmpty();
        if (target.getClass().isArray()) return Array.getLength(target) == 0;

        return false;
    }

    /**
     * 두 객체 내용이 같은지 비교한다.
     *
     * @param a 기준 객체.
     * @param b 비교 객체.
     * @return 내용 동일 여부.
     */
    public static final boolean eq(final Object a, final Object b) {
        return (a == null) ? (b == null) : a.equals(b);
    }

    private O() {}
}
