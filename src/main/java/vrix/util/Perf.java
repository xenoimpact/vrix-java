package vrix.util;

import java.util.concurrent.TimeUnit;

/**
 * 성능 측정.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public final class Perf {
    /**
     * 실행 시간 측정.
     *
     * @param job 실행 대상.
     * @return 실행 시간. (Milliseconds)
     */
    public static final long time(final Runnable job) {
        return time(job, TimeUnit.MILLISECONDS);
    }

    /**
     * 실행 시간 측정.
     *
     * @param job      실행 대상.
     * @param timeUnit 측정 시간 단위.
     * @return 실행 시간.
     */
    public static final long time(final Runnable job,
                                  final TimeUnit timeUnit) {
        V.noNull(job, "job");
        V.noNull(timeUnit, "timeUnit");
        final long s = System.nanoTime();
        job.run();
        return timeUnit.convert(System.nanoTime() - s, TimeUnit.NANOSECONDS);
    }

    /**
     * 반복 실행 시간 측정.
     *
     * @param job    실행 대상.
     * @param repeat 반복 횟수.
     * @return 실행 시간. (Milliseconds)
     */
    public static long[] time(final Runnable job,
                              final int repeat) {
        return time(job, 0, repeat, TimeUnit.MILLISECONDS);
    }

    /**
     * 반복 실행 시간 측정.
     *
     * @param job      실행 대상.
     * @param repeat   반복 횟수.
     * @param timeUnit 측정 시간 단위.
     * @return 실행 시간.
     */
    public static final long[] time(final Runnable job,
                                    final int repeat,
                                    final TimeUnit timeUnit) {
        return time(job, 0, repeat, timeUnit);
    }

    /**
     * 반복 실행 시간 측정.
     *
     * @param job    실행 대상.
     * @param warm   워밍업 횟수.
     * @param repeat 반복 횟수.
     * @return 실행 시간. (Milliseconds)
     */
    public static final long[] time(final Runnable job,
                                    final int warm,
                                    final int repeat) {
        return time(job, warm, repeat, TimeUnit.MILLISECONDS);
    }

    /**
     * 반복 실행 시간 측정.
     *
     * @param job      실행 대상.
     * @param warm     워밍업 횟수.
     * @param repeat   반복 횟수.
     * @param timeUnit 측정 시간 단위.
     * @return 실행 시간.
     */
    public static final long[] time(final Runnable job,
                                    final int warm,
                                    final int repeat,
                                    final TimeUnit timeUnit) {
        V.noNull(job, "job");
        V.noLess(warm, 0, "warm");
        V.noLess(repeat, 1, "repeat");
        V.noNull(timeUnit, "timeUnit");

        final long[] r = new long[repeat];
        long s;
        for (int i = warm + repeat - 1; i >= 0; --i) {
            if (i >= repeat) {
                s = System.nanoTime();
                job.run();
                timeUnit.convert(System.nanoTime() - s, TimeUnit.NANOSECONDS);
            } else {
                s = System.nanoTime();
                job.run();
                r[repeat - i - 1] = timeUnit.convert(System.nanoTime() - s, TimeUnit.NANOSECONDS);
            }
        }

        return r;
    }

    private Perf() {}
}
