package vrix.util;

import org.slf4j.Logger;

/**
 * 문자열 관련 지원 기능 모음.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public final class StringU {
    private static final Logger L = LogHelper.get();

    /**
     * 개행 문자.
     *
     * @return 개행 문자.
     */
    public static final String lineSeparator() {
        return System.getProperty("line.separator");
    }

    /**
     * 문자열의 크기를 고정하고 왼쪽을 빈 문자로 채운다.
     *
     * @param source 문자열.
     * @param size 문자열 크기.
     * @return 채워진 문자열.
     */
    public static final String lpad(final String source,
                                    final int size) {
        return lpad(source, size, ' ');
    }

    /**
     * 문자열의 크기를 고정하고 왼쪽을 지정 문자로 채운다.
     * 
     * @param source 문자열.
     * @param size 문자열 크기.
     * @param padding 채울 지정 문자.
     * @return 채워진 문자열.
     * @throws IllegalArgumentException
     */
    public static final String lpad(final String source,
                                    final int size,
                                    final char padding) throws IllegalArgumentException {
        V.noLess(size, 0, "size");
        if (size == 0) return "";
        
        final char[] sourceChars = (source != null) ? source.toCharArray() : new char[0];
        final int sourceSize = sourceChars.length;
        if (sourceSize >= size) return source;
        
        final char[] result = new char[size];
        for (int i = size - 1, sourceCursor = sourceSize - 1;
             i >= 0;
             --i, --sourceCursor) {
            if (sourceCursor >= 0) result[i] = sourceChars[sourceCursor];
            else result[i] = padding;
        }
        
        return new String(result);
    }

    /**
     * 문자열의 크기를 고정하고 오른쪽을 빈 문자로 채운다.
     *
     * @param source 문자열.
     * @param size 문자열 크기.
     * @return 채워진 문자열.
     */
    public static final String rpad(final String source,
                                    final int size) {
        return rpad(source, size, ' ');
    }

    /**
     * 문자열의 크기를 고정하고 오른쪽을 지정 문자로 채운다.
     * @param source 문자열.
     * @param size 문자열 크기.
     * @param padding 채울 지정 문자.
     * @return 채워진 문자열.
     */
    public static final String rpad(final String source,
                                    final int size,
                                    final char padding) {
        V.noLess(size, 0, "size");
        if (size == 0) return "";

        final char[] sourceChars = (source != null) ? source.toCharArray() : new char[0];
        final int sourceSize = sourceChars.length;
        if (sourceSize >= size) return source;

        final char[] result = new char[size];
        for (int i = 0, sourceCursor = 0;
             i < size;
             ++i, ++sourceCursor) {
            if (sourceCursor < sourceSize) result[i] = sourceChars[sourceCursor];
            else result[i] = padding;
        }

        return new String(result);
    }

    private StringU() {}
}
