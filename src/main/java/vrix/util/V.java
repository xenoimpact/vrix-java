package vrix.util;

/**
 * 검증 관련 지원 기능 모음.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public final class V {
    /**
     * null 방지.
     * 검증 대상이 null인 경우 예외를 발생시킨다.
     *
     * @param target 검증 대상.
     * @param targetName 대상명.
     * @throws IllegalArgumentException
     */
    public static final void noNull(final Object target, final String targetName) throws IllegalArgumentException {
        if (targetName == null) throw new IllegalArgumentException("`targetName` must not be null.");
        if (target == null) throw new IllegalArgumentException(
                new StringBuilder("`")
                        .append(targetName)
                        .append("` must not be null.")
                        .toString()
        );
    }

    /**
     * 빈 객체 방지.
     * 검증 대상이 비어있는 경우 예외를 발생시킨다.
     *
     * @param target 검증 대상.
     * @param targetName 대상명.
     * @throws IllegalArgumentException
     */
    public static final void noEmpty(final Object target, final String targetName) throws IllegalArgumentException {
        noNull(target, targetName);
        if (O.empty(target)) throw new IllegalArgumentException(
                new StringBuilder("`")
                        .append(targetName)
                        .append("` must not be empty.")
                        .toString()
        );
    }

    /**
     * @deprecated Use `O.empty(Object)` instead.
     */
    @Deprecated
    public static final boolean empty(final Object target) {
        return O.empty(target);
    }

    /**
     * 기준보다 작은 값 방지.
     *
     * @param target 대상 값.
     * @param noLessThan 기준 값.
     * @throws IllegalArgumentException
     */
    public static final void noLess(final int target, final int noLessThan, final String targetName) throws IllegalArgumentException {
        if (target < noLessThan) throwNoLess(targetName, String.valueOf(noLessThan));
    }

    private static final void throwNoLess(final String targetName, final String noLessThanString) throws IllegalArgumentException {
        V.noEmpty(targetName, "targetName");
        throw new IllegalArgumentException(new StringBuilder("`")
                .append(targetName)
                .append("` must not be less than ").append(noLessThanString).append('.')
                .toString()
        );
    }

    /**
     * 기준보다 큰 값 방지.
     *
     * @param target 대상 값.
     * @param noGreaterThan 기준 값.
     * @throws IllegalArgumentException
     */
    public static final void noGreater(final int target, final int noGreaterThan, final String targetName) throws IllegalArgumentException {
        if (target > noGreaterThan) throwNoGreater(targetName, String.valueOf(noGreaterThan));
    }

    private static final void throwNoGreater(final String targetName, final String noGreaterThanString) throws IllegalArgumentException {
        V.noEmpty(targetName, "targetName");
        throw new IllegalArgumentException(new StringBuilder("`")
                .append(targetName)
                .append("` must not be greater than ").append(noGreaterThanString).append('.')
                .toString()
        );
    }

    private V() {}
}
