package vrix.data.graphic;

import org.junit.Assert;
import org.junit.Test;

/**
 * 색상 테스트.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public class ColorTest {
    @Test
    public void testColor() {
        Color color = new Color(0, 191, 255);
        Assert.assertEquals("00bfff".toUpperCase(), color.hex().toUpperCase());
        Assert.assertEquals(49151, color.rgb());

        color = new Color(153, 51, 102);
        Assert.assertEquals("993366".toUpperCase(), color.hex().toUpperCase());
        Assert.assertEquals(10040166, color.rgb());
    }
}
