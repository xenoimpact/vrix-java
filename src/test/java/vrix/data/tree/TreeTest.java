package vrix.data.tree;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 트리 기능 테스트.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public class TreeTest {
    NodeMock node1 = new NodeMock("1", null);
    NodeMock node2 = new NodeMock("1-1", "1");
    NodeMock node3 = new NodeMock("1-2", "1");
    NodeMock node4 = new NodeMock("2", null);
    NodeMock node5 = new NodeMock("2-1", "2");
    NodeMock node6 = new NodeMock("2-2", "2");
    NodeMock node7 = new NodeMock("2-2-1", "2-2");
    NodeMock node8 = new NodeMock("2-2-2", "2-2");
    NodeMock node9 = new NodeMock("3", null);
    NodeMock node10 = new NodeMock("4", null);
    NodeMock node11 = new NodeMock("4-1", "4");
    NodeMock node12 = new NodeMock("4-2", "4");
    NodeMock node13 = new NodeMock("4-3", "4");
    NodeMock node14 = new NodeMock("4-3-1", "4-3");
    NodeMock node15 = new NodeMock("4-3-2", "4-3");
    NodeMock node16 = new NodeMock("4-3-3", "4-3");
    NodeMock node17 = new NodeMock("4-3-4", "4-3");
    NodeMock node18 = new NodeMock("4-4", "4");
    NodeMock node19 = new NodeMock("4-4-1", "4-4");
    NodeMock node20 = new NodeMock("4-4-2", "4-4");
    NodeMock node21 = new NodeMock("4-4-3", "4-4");
    NodeMock node22 = new NodeMock("4-4-4", "4-4-a");
    NodeMock node23 = new NodeMock("4-4-5", "4-4-a");
    NodeMock node24 = new NodeMock("4-4-6", "4-4-a");
    NodeMock node25 = new NodeMock("4-4-7", "4-4-a");

    List<NodeMock> nodes = Arrays.asList(
            node1, node2, node3, node4, node5, node6, node7, node8, node9, node10,
            node11, node12, node13, node14, node15, node16, node17, node18, node19, node20,
            node21, node22, node23, node24, node25
    );

    @Test
    public void testTreeFrom() {
        final Tree<NodeMock, String> tree = Tree.from(nodes, NodeMock.NODE_HELPER);
        Assert.assertEquals(8, tree.root().size());
        Assert.assertEquals(2, tree.children(tree.get("1")).size());
        Assert.assertEquals(2, tree.children(tree.get("2")).size());
        Assert.assertEquals(2, tree.children(tree.get("2-2")).size());
        Assert.assertEquals(0, tree.children(tree.get("3")).size());
        Assert.assertEquals(4, tree.children(tree.get("4")).size());
        Assert.assertEquals(4, tree.children(tree.get("4-3")).size());
        Assert.assertEquals(3, tree.children(tree.get("4-4")).size());
    }

    @Test
    public void testLeaf() {
        final Tree<NodeMock, String> tree = Tree.from(nodes, NodeMock.NODE_HELPER);
        Assert.assertEquals(19, tree.leaf().size());
        NodeMock.NODE_HELPER.children(tree.get("4-4-6")).add(new NodeMock("4-4-6-1", "4-4-7"));
        NodeMock.NODE_HELPER.children(tree.get("4-4-6")).add(new NodeMock("4-4-6-2", "4-4-6"));
        NodeMock.NODE_HELPER.children(tree.get("4-4-6")).add(new NodeMock("4-4-6-3", "4-4-6"));
        NodeMock.NODE_HELPER.children(tree.get("4-4-7")).add(new NodeMock("4-4-7-1", "4-4-7"));
        NodeMock.NODE_HELPER.children(tree.get("4-4-7")).add(new NodeMock("4-4-7-2", "4-4-7"));
        NodeMock.NODE_HELPER.children(tree.get("4-4-7")).add(new NodeMock("4-4-7-3", "4-4-7"));
        Assert.assertEquals(19, tree.leaf().size());
        tree.reload();
        Assert.assertEquals(23, tree.leaf().size());
        Assert.assertEquals(3, tree.leaf(node24).size());
    }

    static final class NodeMock {
        NodeMock(String id, String parentId) {
            super();
            this.id = id;
            this.parentId = parentId;
        }

        String id;
        String parentId;
        List<NodeMock> children;

        @Override
        public String toString() {
            return new StringBuilder("{id=").append(id)
                    .append(", parentId=").append(parentId).append("}")
                    .toString();
        }

        private static final NodeHelper<NodeMock, String> NODE_HELPER = new NodeHelper<NodeMock, String>() {
            @Override
            public String id(NodeMock nodeMock) {
                return nodeMock.id;
            }

            @Override
            public String parentId(NodeMock nodeMock) {
                return nodeMock.parentId;
            }

            @Override
            public boolean isRoot(NodeMock nodeMock) {
                return nodeMock.parentId == null;
            }

            @Override
            public List<NodeMock> children(NodeMock nodeMock) {
                if (nodeMock.children == null) nodeMock.children = new ArrayList<NodeMock>();
                return nodeMock.children;
            }

            @Override
            public void children(NodeMock nodeMock, List<NodeMock> children) {
                nodeMock.children = children;
            }
        };
    }
}
