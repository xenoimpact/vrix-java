package vrix.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * 성능 측정 테스트.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public class PerfTest {
    @Test
    public void testTime() {
        final long[] time = Perf.time(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {}
            }
        }, 10, 10);
        Assert.assertEquals(10, time.length);
    }
}