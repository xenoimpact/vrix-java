package vrix.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * 문자열 관련 지원 기능 테스트.
 *
 * @author jy.choi@xenoimpact.com
 * @since 2017
 */
public class StringUTest {
    @Test
    public void testLpad() {
        final String source = "14531";
        Assert.assertEquals(source, StringU.lpad(source, 2, '0'));
        Assert.assertEquals("014531", StringU.lpad(source, 6, '0'));
        Assert.assertEquals("0000014531", StringU.lpad(source, 10, '0'));
    }

    @Test
    public void testRpad() {
        final String source = "14531";
        Assert.assertEquals(source, StringU.rpad(source, 2, '0'));
        Assert.assertEquals("145310", StringU.rpad(source, 6, '0'));
        Assert.assertEquals("1453100000", StringU.rpad(source, 10, '0'));
    }
}
